<?php
include_once "vendor/autoload.php";
use Pondit\Institute\Student;
use Pondit\Institute\Teacher;
use Pondit\Institute\Mark;
use Pondit\Institute\Group;
use Pondit\Institute\Subject;

$student=new Student();
echo "Student Name: ";
echo  $student->studentName;
echo "<br>";
echo "Teacher Name: ";
$teacher=new Teacher();
echo $teacher->teacherName;
echo "<br>";
echo "Group Name: ";
$group=new Group();
echo $group->groupName;
echo "<br>";
echo "Subject Name: ";
$subject=new Subject();
echo $subject->subjectName;
echo "<br>";
$mark=new Mark();
echo "Mark : ";
echo $mark->mark;